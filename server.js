//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var path = require('path');
var cors = require('cors'):

app.use(express.static(__dirname + "/build/default"));

app.use(function(req, res, next){         
	res.header("Access-Control-Allow-Origin", "*");         
	res.header("Access-Control-Allow-Headers", "*");         
	res.header("Access-Control-Allow-Methods", "*");         
	res.header("Access-Control-Max-Age", "86400");         
	next(); 
});

app.use(cors());

app.listen(port);

console.log('Polymer over NodeJS server started on: ' + port);

app.get("/", function(req, res) {
  res.sendFile('index.html', { root: '.' });
});
